<?php

function enqueueScriptsAndCss () {
  wp_enqueue_script( 'AngularJs', 'https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js', false, false, false);
  wp_enqueue_script( 'AngularSanitaze', 'https://code.angularjs.org/1.6.6/angular-sanitize.js', false, false, false);
  wp_enqueue_script( 'MainJs', get_template_directory_uri() . '/js/postCtrl.js', false, false, false);
  wp_enqueue_script( 'ServiceJs', get_template_directory_uri() . '/js/service.js', false, false, false);
  wp_enqueue_script( 'onHover', get_template_directory_uri() . '/js/onHover.js', false, false, false);

  wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css');
  wp_enqueue_style( 'main-css', get_template_directory_uri() . '/css/style.css');
}
add_action( 'wp_enqueue_scripts', 'enqueueScriptsAndCss' );

add_theme_support( 'post-thumbnails' );
add_theme_support( 'post-thumbnails', array( 'post' ) );          // Posts only
add_theme_support( 'post-thumbnails', array( 'page' ) );          // Pages only
add_theme_support( 'post-thumbnails', array( 'post', 'movie' ) ); // Posts and Movies
