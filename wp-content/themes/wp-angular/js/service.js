angular.module('myApp')
.factory('service', ['$http', '$q', function($http, $q) {
    return {
      getPosts: function() {
        var deferred = $q.defer();
        $http.get('http://localhost/wp-json/wp/v2/posts?_embed')
        .then(function(response){
           deferred.resolve(response.data);
        }, function(error){
          deferred.reject(error);
        });
        return deferred.promise;
      }
    }
}]);
