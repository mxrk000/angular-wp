angular.module('myApp', ['ngSanitize'])
.controller('postCtrl', ['$scope', 'service', function($scope, service) {
    service.getPosts()
    .then(function(response){
      $scope.posts = response;
    }, function(error){
      console.log(error);
    });
}]);
