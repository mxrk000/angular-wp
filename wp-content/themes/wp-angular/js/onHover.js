angular.module('myApp')
.directive('onHover', function(){
  return {
    restrict: 'A',
    link: function ($scope, element, attrs) {
      element.on('mouseenter', function () {
          element.css({
            '-webkit-filter': 'grayscale(0%)',
            'filter': 'grayscale(0%)',
            'transition': '.3s filter ease-out'
          });
      });
      element.on('mouseleave', function () {
          element.css({
            '-webkit-filter': 'grayscale(100%)',
            'filter': 'grayscale(100%)',
          });
      });
    }
  }
});
