<?php get_header(); ?>
<div class="container">
  <h1 class="text-center text-primary">AngularJs - Wordpress</h1>
  <div class="row">
    <div ng-repeat="post in posts" class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
      <figure class="text-center">
        <img on-hover ng-src="{{post._embedded['wp:featuredmedia'][0].source_url}}" alt="">
      </figure>
      <h3 ng-bind-html="post.title.rendered" class="text-center"></h3>
      <p ng-bind-html="post.content.rendered"></p>
    </div>
  </div>
</div>
<?php get_footer(); ?>
